# Comprendre la notion de DNS sous K8

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Concept

Nous allons parler du DNS dans Kubernetes. Voici un aperçu rapide de ce dont nous allons discuter :

- Le concept de DNS dans Kubernetes
- Les noms de domaine des pods
- Une démonstration pratique du DNS dans Kubernetes

# Concept de DNS dans Kubernetes

Le réseau virtuel de Kubernetes inclut un système de noms de domaine (DNS) qui permet aux pods de localiser d'autres pods et services en utilisant des noms de domaine au lieu d'adresses IP. Ce DNS fonctionne comme un service au sein du cluster. Il s'exécute en tant que pod, exposé via un service que vous pouvez généralement trouver dans le namespace `kube-system`. Dans notre cas, nous utilisons un cluster `kubeadm`, qui utilise CoreDNS comme solution DNS.

# Noms de Domaine des Pods

Avec ce DNS spécial, tous les pods de votre cluster reçoivent automatiquement un nom de domaine qui ressemble à l'exemple ci-dessous. 

```bash
pod-ip-address.namespace-name.pod.cluster.local
```

Il inclut l'adresse IP du pod avec les points remplacés par des tirets, suivi de `default.pod.cluster.local`. Voici un exemple de ce à quoi ressemble un nom de domaine DNS d'un pod :

```bash
192-168-103-25.default.pod.cluster.local
```

Chaque pod peut communiquer avec un autre pod en utilisant ce nom de domaine. Bien que les noms de domaine DNS ne soient pas très utiles pour la communication entre pods individuels (puisque vous devez déjà connaître l'adresse IP), ils deviennent beaucoup plus utiles lorsque nous commençons à utiliser des services.

# Démonstration Pratique

Connectons-nous à notre serveur de plan de contrôle Kubernetes et explorons le DNS dans Kubernetes.

1. Créez un fichier appelé `dnstest-pods.yml` :
```bash
nano dnstest-pods.yml
```

2. Ajoutez ce qui suit au fichier pour créer deux pods :
   ```yaml
   ---
   apiVersion: v1
   kind: Pod
   metadata:
     name: busybox-dnstest
   spec:
     containers:
     - name: busybox
       image: busybox
       command: ['sh', '-c', 'sleep 3600']
   ---
   apiVersion: v1
   kind: Pod
   metadata:
     name: nginx-dnstest
   spec:
     containers:
     - name: nginx
       image: nginx
       ports:
       - containerPort: 80
   ```

3. Créez les pods à partir de ce fichier :
   ```bash
   kubectl apply -f dnstest-pods.yml
   ```

4. Obtenez l'adresse IP du pod `nginx-dnstest` :

```bash
kubectl get pods -o wide
```

5. Exécutez une commande dans le pod `busybox-dnstest` pour accéder au pod `nginx-dnstest` en utilisant son adresse IP :

```bash
kubectl exec -it busybox-dnstest -- curl <IP_ADDRESS>
```

6. Obtenez les pods dans le namespace `kube-system` pour voir les pods CoreDNS :

```bash
kubectl get pods -n kube-system
```

7. Obtenez les services dans le namespace `kube-system` pour voir le service kube-dns :

```bash
kubectl get svc -n kube-system
```

8. Exécutez une commande `nslookup` à l'intérieur du pod `busybox-dnstest` pour rechercher le nom de domaine DNS du pod `nginx-dnstest` :

```bash
kubectl exec -it busybox-dnstest -- nslookup 192-168-103-25.default.pod.cluster.local
   ```

9. Faites une requête `curl` en utilisant le nom de domaine DNS :

```bash
kubectl exec -it busybox-dnstest -- curl 192-168-103-25.default.pod.cluster.local
   ```

# Conclusion

Pour récapituler, nous avons parlé du concept de DNS dans Kubernetes, des noms de domaine des pods, et nous avons fait une démonstration pratique pour explorer ces concepts dans notre cluster. C'est tout pour cette leçon. À la prochaine !


# Reférence

https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/